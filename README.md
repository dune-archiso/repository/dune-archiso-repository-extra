# [`dune-archiso-repository-extra`](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra) packages for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra/-/raw/main/packages.x86_64). Most of the packages are installed from the ISO image, no DUNE modules are found here.

## [Usage](https://gitlab.com/dune-archiso/repository/dune-archiso-repository-extra/-/pipelines/latest)

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Append the following lines to `/etc/pacman.conf`:

```toml
[dune-archiso-repository-extra]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/dune-archiso-repository-extra/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-archiso-repository-extra
```

### Install packages

To install package:

```console
[user@hostname ~]$ sudo pacman -Syu
[user@hostname ~]$ sudo pacman -S emacs-git
```
